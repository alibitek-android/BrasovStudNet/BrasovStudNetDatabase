﻿-- Function: api.get_registration_token(text)

-- DROP FUNCTION api.get_registration_token(text);

CREATE OR REPLACE FUNCTION api.get_registration_token(email text)
  RETURNS text AS
$BODY$
	query = plpy.prepare('select id from student_enrollments s where email = $1', ['text'])
	result = plpy.execute(query, [email])

	if result:
		query = plpy.prepare('select token from user_tokens ut join users u on ut.user_id = u.id where u.user_id = $1 and role_id = 1', ['integer'])
		result = plpy.execute(query, [result[0]['id']])		
	else:
		query = plpy.prepare('select id from faculties_staff s where email = $1', ['text'])
		result = plpy.execute(query, [email])
		
		if result:
			query = plpy.prepare('select token from user_tokens ut join users u on ut.user_id = u.id where u.user_id = $1 and role_id = 2', ['integer'])
			result = plpy.execute(query, [result[0]['id']])

	if result:
		return result[0]['token']
	else:
		return None
$BODY$
  LANGUAGE plpython3u VOLATILE
  COST 100;
ALTER FUNCTION api.get_registration_token(text)
  OWNER TO postgres;
