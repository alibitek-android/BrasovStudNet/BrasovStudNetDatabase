﻿-- Function: api.upsert_token(text, text)

-- DROP FUNCTION api.upsert_token(text, text);

CREATE OR REPLACE FUNCTION api.upsert_token(
    ptoken text,
    pemail text)
  RETURNS boolean AS
$BODY$

declare uid integer = api.get_user_id(pemail);

BEGIN
	-- TODO: Need to implement this method by taking into consideration that as user can have multiple devices, which means a token per device not per user
	--RAISE NOTICE '%', uid;

	if uid is null then
		return false;
	end if;
	
	LOOP
		-- first try to update the key
		UPDATE user_tokens SET token = ptoken, utime = now() WHERE user_id = uid;
		IF found THEN
		    RETURN true;
		END IF;
		
		-- not there, so try to insert the key
		-- if someone else inserts the same key concurrently,
		-- we could get a unique-key failure
		BEGIN
		    INSERT INTO user_tokens(user_id, token) VALUES (uid, ptoken);
		    RETURN true;
		EXCEPTION WHEN unique_violation THEN
		    -- do nothing, and loop to try the UPDATE again
		END;
	END LOOP;

	RETURN false;
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION api.upsert_token(text, text)
  OWNER TO postgres;

--select api.upsert_token('3', 'alexandru.butum@student.unitbv.ro')
--select api.get_user_id('alexandru.butum@student.unitbv.ro')
