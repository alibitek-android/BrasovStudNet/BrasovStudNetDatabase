﻿-- Function: api.get_user_type_id(text)

-- DROP FUNCTION api.get_user_type_id(text);

CREATE OR REPLACE FUNCTION api.get_user_type_id(pemail text)
  RETURNS integer AS
$BODY$
declare tmp integer;
BEGIN
	select id into tmp from student_enrollments where email = pemail;

	if tmp is not null
	then
		select id into tmp from roles where name = 'student';
	else
		select id into tmp from roles where name = 'teacher';
	end if;

	return tmp;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION api.get_user_type_id(text)
  OWNER TO postgres;
