﻿-- Function: api.get_user_id(text)

-- DROP FUNCTION api.get_user_id(text);

CREATE OR REPLACE FUNCTION api.get_user_id(pemail text)
  RETURNS integer AS
$BODY$
declare v_id integer;
declare v_type integer default api.get_user_type_id(pemail);
BEGIN

	case when v_type = 1 then 
		         select u.id into v_id from users u join student_enrollments e on e.id = u.user_id where role_id = v_type and email = pemail;
		    when v_type = 2 then
			 select f.id into v_id from users u join faculties_staff f on u.user_id = f.id join faculties_jobs j on f.job_id = j.id 
					where j.name in ('PROFESOR UNIV.', 'CONFERENTIAR UNIV.', 'LECTOR UNIVERSITAR') and email = pemail;
	end case;

	return v_id;

END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION api.get_user_id(text)
  OWNER TO postgres;
