﻿-- Function: before_insert_faculties_staff()

-- DROP FUNCTION before_insert_faculties_staff();

CREATE OR REPLACE FUNCTION before_insert_faculties_staff()
  RETURNS trigger AS
$BODY$

DECLARE faculty_id integer default 0;
DECLARE department_id integer default 0;

BEGIN
	RAISE NOTICE '%s', 'here1';
	SELECT id INTO faculty_id FROM faculties where NEW.faculty_id ilike '%' || name || '%';
	IF faculty_id <> 0 THEN
		NEW.faculty_id = faculty_id;
	END IF;
	RAISE NOTICE '%s', 'here2';

	SELECT id INTO department_id FROM departments d where d.faculty_id = faculty_id;
	IF department_id <> 0 THEN
		NEW.department_id = department_id;
	END IF;

	RETURN NEW;
 END;
 
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION before_insert_faculties_staff()
  OWNER TO postgres;
