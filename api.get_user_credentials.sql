﻿-- Function: api.get_user_credentials(text)

-- DROP FUNCTION api.get_user_credentials(text);

CREATE OR REPLACE FUNCTION api.get_user_credentials(IN pemail text)
  RETURNS TABLE(username character varying, password character varying, enabled boolean) AS
$BODY$BEGIN

return query (select (select case when api.get_user_type_id(pemail) = 1 then 
				(select email from student_enrollments where email = pemail)
                           when api.get_user_type_id(pemail) = 2 then 
				(select email from faculties_staff f join faculties_jobs j on f.job_id = j.id 
					where j.name in ('PROFESOR UNIV.', 'CONFERENTIAR UNIV.', 'LECTOR UNIVERSITAR') and email = pemail)
		      end) as username, 
				password_hash as password, 
				u.enabled
		from users u where id = (select id from users where id = api.get_user_id(pemail)));

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION api.get_user_credentials(text)
  OWNER TO postgres;
