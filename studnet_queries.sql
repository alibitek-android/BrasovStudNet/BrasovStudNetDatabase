﻿sudo ufw allow from 192.168.1.0/24 to any port 8000

sudo apt-get install postgresql-contrib-9.4
http://www.postgresql.org/docs/current/static/contrib.html

CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION pgcrypto;
CREATE EXTENSION plpython3u;
CREATE EXTENSION unaccent;

SELECT * FROM pg_available_extensions;

-- Generate users
insert into users(user_id, password_hash, role_id) select id, crypt('unitbv', gen_salt('bf', 8)), (select id from roles where name = 'student') from student_enrollments where id >= 181; 
insert into users(user_id, password_hash, role_id) select f.id, crypt('unitbv', gen_salt('bf', 8)), (select id from roles where name = 'teacher') from faculties_staff f join faculties_jobs j on f.job_id = j.id and j.name in ('PROFESOR UNIV.', 'CONFERENTIAR UNIV.', 'LECTOR UNIVERSITAR'); 

insert into users(user_id, password_hash, role_id) select id, crypt('unitbv', gen_salt('bf', 8)), (select id from roles where name = 'student') from student_enrollments where student_name = 'asda'; 
insert into users(user_id, password_hash, role_id) select f.id, crypt('unitbv', gen_salt('bf', 8)), (select id from roles where name = 'teacher') from faculties_staff f where email = 'asd';

SELECT crypt('unitbv', password_hash) as pwd_hash from users u join student_enrollments e on e.id = u.user_id join roles r on r.id = u.role_id and email = 'alexandru.butum@student.unitbv.ro' and r.name = 'student'

select e.id, u.id, u.user_id, role_id from users u join student_enrollments e on e.id = u.user_id join roles r on r.id = u.role_id 
where email = 'alexandru.butum@student.unitbv.ro' and role_id = api.get_user_type_id('alexandru.butum@student.unitbv.ro')

select (case when api.get_user_type_id(email) = 1 then select email from student_enrollments where email = ?
             when api.get_user_type_id(email) = 2 then select email from faculties_staff f join faculties_jobs j on f.job_id = j.id and j.name in ('PROFESOR UNIV.', 'CONFERENTIAR UNIV.', 'LECTOR UNIVERSITAR') where email = ?) as username, 
password_hash as password, 
enabled
from users where email=? and role_id = api.get_user_type_id(?)

select e.id, u.id, u.user_id, role_id from student_enrollments e join users u on e.id = u.id join roles r on r.id = u.role_id 
where email = 'sdadsa'

select * from faculties_staff e join users u on e.id = u.user_id join roles r on r.id = u.role_id 
where email = 'ogabriel@unitbv.ro'
update users set enabled = true

select *
from (select u.id, u.user_id, u.role_id from users u join faculties_staff e on e.id = u.user_id where email = 'd.bocu@unitbv.ro') u
where u.role_id = (select id from roles where id = u.role_id)

select * from users where password_hash = crypt('unitbv', (select));

SELECT encode(digest('blah', 'sha512'), 'hex');

select gen_random_uuid();

http://www.postgresonline.com/journal/archives/165-Encrypting-data-with-pgcrypto.html

select to_json(x) from (select e.name, facultatea, ciclul_de_invatamant, forma_de_invatamant, domeniul, an_de_studiu, programul_de_studii, grupa from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id where email = 'Alexandru.Butum@student.unitbv.ro' and r.name = 'student') x

 

select username, password, enabled from api.get_user_credentials('alexandru.butum@student.unitbv.ro')

-- User profile
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/users/profile

-- Students
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/users/students

-- Teachers
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/users/teachers

-- Students in topic(group)
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/users/students/topic?topicAddress=934385f53d1bd0c1b8493e44d0dfd4c8e88a04bb

-- Register token
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --form POST http://localhost:8000/api/1/tokens token=test
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json POST http://localhost:8000/api/1/tokens value=test

-- Events
http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/events 

http --auth alexandru.butum@student.unitbv.ro:unitbv --auth-type basic --json GET http://localhost:8000/api/1/events/year/2015/month/7

select name, description, start_at, end_at from events_users us join events e on e.id = us.event_id join users u on u.id = us.user_id where u.id = api.get_user_id('alexandru.butum@student.unitbv.ro') and date_part('year', start_at) = date_part('year', now()) and date_part('month', start_at) = date_part('month', now())

select date_part('month', '2015-07-01 13:00:00 +3:00'::timestamp)

select * from topics where id in (496, 497, 498, 499, 500, 501, 502, 503, 504);

name = {String@5061} "campus_entire_topic"
type = {String@4345} "STUDENT_GROUP_CAMPUS"

select * from api.get_user_credentials('alexandru.butum@student.unitbv.ro')
select * from api.get_user_roles('alexandru.butum@student.unitbv.ro')

select * From users where password_hash = '$2a$08$R2t3bfgmuqvrGyk3qmkvjeHtibMk0xO4HlpXINC5nRLlfzEYncona'

insert into user_tokens (user_id, token) values ((select u.id from users u join student_enrollments e on u.user_id = e.id join roles r on u.role_id = r.id and r.name = 'student' where email = 'alexandru.butum@student.unitbv.ro'), 'test')

(select u.id from users u join student_enrollments e on u.user_id = e.id join roles r on u.role_id = r.id and r.name = 'student' where email = 'alexandru.butum@student.unitbv.ro')

update topics set type_id = 1 where name ilike 'school_%'
update topics set type_id = 2 where name not ilike 'school_%'

select committee_chairman from campuses where id = 10
       
select t.name, t.address, (select type from topic_types where id = t.type_id) from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id 
join users_topics ut on ut.user_id = u.id join topics t on t.id = ut.topic_id
where email = lower('alexandru.butum@student.unitbv.ro') and r.name = 'student'

select encode(digest('7932@student.unitbv.ro', 'sha1'), 'hex')

-- Generate hashes
update faculties_staff set email_hash = encode(digest(email, 'sha1'), 'hex')
update student_enrollments set email_hash = encode(digest(email, 'sha1'), 'hex')

Campus

School
- Group mates: 7932@student.unitbv.ro
- School mates: 

'MATEMATICA SI INFORMATICA' | 'Master' | 'Zi' | 'Informatica' | 'II' || 'Tehnologii moderne in ingineria sistemelor soft'

select substr('312'::text, 1, 1)

insert into topics(name, address)
values
('room_topic', '380247e2713ea05e9bc001ebe20c2bb498cd476b'),
('floor_topic', '934385f53d1bd0c1b8493e44d0dfd4c8e88a04bb'),
('dorm_topic', 'b1d5781111d84f7b3fe45a0852e59758cd7a87e5'),
('campus_topic', '26c219c1ba38893a25d023060614b484b4b1ffe7'),
('school_group_topic', '754d4dac04735469c6447fe7f6bec11d9db28f54'),
('school_class_topic', 'f463f733ab5b5827881a491080c00e107be0b5c2'),
('school_class_entire_year_topic', 'c815e63104c804683493491b68cf0387f03cffea'),
('school_entire_cicle_topic', '9c36e2076f0f7039af3ec56b15ef3758c1564587'),
('school_entire_faculty_topic', '86973d57354e4c701cd2f3c8e910e3de84904684')

insert into users_topics(user_id, topic_id) values 
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9)

select u.id, e.email from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id and r.name = 'student'

select u.id, e.email from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id 
where email = lower('alexandru.butum@student.unitbv.ro') and r.name = 'student'

select id from topics where id = ANY($1)

with update_topics as (update topics set address = '380247e2713ea05e9bc001ebe20c2bb498cd476b' where address = '380247e2713ea05e9bc001ebe20c2bb498cd476b' returning id)
insert into topics (name, address, type_id) 
select 'campus_room_topic', '380247e2713ea05e9bc001ebe20c2bb498cd476b', (select id from topic_types where type = split_part('campus_room_topic', '_', 1)) where not exists (select * from update_topics) 

select 
(select encode(digest(campus_number::text || room_number::text, 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as room_topic,
(select encode(digest(campus_number::text || substr(room_number::text, 1, 1), 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as floor_topic,
(select encode(digest(campus_number::text, 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as dorm_topic, 
(select encode(digest((select complex from campuses where id = campus_number), 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as campus_topic, 
encode(digest(facultatea || grupa::text, 'sha1'), 'hex') as school_group_topic,
encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant || domeniul || an_de_studiu || programul_de_studii, 'sha1'), 'hex') as school_class_topic,
encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant || domeniul || an_de_studiu, 'sha1'), 'hex') as school_class_entire_year_topic,
encode(digest(facultatea, 'sha1'), 'hex') as school_entire_faculty_topic,
encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant, 'sha1'), 'hex') as school_entire_cicle_topic
from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id 
where email = lower('alexandru.butum@student.unitbv.ro') and r.name = 'student'


insert into user_tokens (user_id, token) values ((select id from users u join student_enrollments e on u.user_id = e.id join roles r on u.role_id = r.id and r.name = 'student' where email = ?), ?)

update student_accomodations set email = CASE WHEN regexp_split_to_array(student_name, E'\\s+')

update student_enrollments set student_name = trim(student_name)

array_length(regexp_split_to_array(student_name, E'\\s+'), 1)

update student_enrollments set student_name = trim(student_name)
update student_accomodations set student_name = trim(student_name)
update student_enrollments set email = lower(email)
update faculties_staff set email = lower(email)

select * from (
select student_name, email as original, CASE WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) = 2 THEN lower(unaccent(split_part(student_name, ' ', 2))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
					     WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) = 3 THEN lower(unaccent(split_part(student_name, ' ', 3))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
					     WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) >= 4 THEN lower(unaccent(split_part(student_name, ' ', 3))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
			  END as email from student_enrollments
) x 
where original <> email

update student_accomodations set email = CASE WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) = 2 THEN lower(unaccent(split_part(student_name, ' ', 2))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
					     WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) = 3 THEN lower(unaccent(split_part(student_name, ' ', 3))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
					     WHEN array_length(regexp_split_to_array(student_name, E'\\s+'), 1) >= 4 THEN lower(unaccent(split_part(student_name, ' ', 3))) || '.' || lower(unaccent(split_part(student_name, ' ', 1))) || '@student.unitbv.ro' 
			  END

http://www.unitbv.ro/Default.aspx?alias=www.unitbv.ro/but&
- topic pentru studentii care au imprumutat aceleasi carti
- topic pentru studentii care merg in aceeasi zi la biblioteca
(recomandari de carti + studentii pe care poti sa-i intalnesti/descoperi pe baza la ce carti imprumuti [categoria de carti, etc])

select t.* from users_topics ut join topics t on ut.topic_id = t.id where user_id = 3;

496;'campus_room_topic';'380247e2713ea05e9bc001ebe20c2bb498cd476b';2
497;'campus_floor_topic';'934385f53d1bd0c1b8493e44d0dfd4c8e88a04bb';2
498;'campus_dorm_topic';'b1d5781111d84f7b3fe45a0852e59758cd7a87e5';2
499;'campus_entire_topic';'26c219c1ba38893a25d023060614b484b4b1ffe7';2
500;'school_group_topic';'754d4dac04735469c6447fe7f6bec11d9db28f54';1
501;'school_class_topic';'f463f733ab5b5827881a491080c00e107be0b5c2';1
502;'school_class_entire_year_topic';'c815e63104c804683493491b68cf0387f03cffea';1
503;'school_entire_faculty_topic';'9c36e2076f0f7039af3ec56b15ef3758c1564587';1
504;'school_entire_cicle_topic';'86973d57354e4c701cd2f3c8e910e3de84904684';1

-- Users from group
select e.student_name, 
email_hash as id, 
email, 
facultatea, 
ciclul_de_invatamant, 
forma_de_invatamant, 
domeniul, 
an_de_studiu, 
programul_de_studii, 
grupa, 
(select campus_number from student_accomodations a where a.email = e.email) as campus_number, 
(select room_number from student_accomodations a where a.email = e.email) as room_number 
from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id
join users_topics ut on ut.user_id = u.id join topics t on ut.topic_id = t.id where r.name = 'student' and t.address = 'b1d5781111d84f7b3fe45a0852e59758cd7a87e5'

insert into student_enrollments (email) 
select distinct on (a.email) a.email from student_accomodations a left join student_enrollments e on a.email = e.email where e.email is null

update student_enrollments set student_name = initcap(split_part(split_part(email, '@', 1), '.', 1)) || ' ' || initcap(split_part(split_part(email, '@', 1), '.', 2)) where id >= 181

update student_accomodations set email = 'i.drd189@student.unitbv.ro' where id = 189

select * from student_accomodations where email = 'i.drd@student.unitbv.ro'

select * from api.generate_topics()

select distinct name from topics order by name


update topics set name = 'Dorm' where name = 'campus_dorm_topic';
update topics set name = 'Campus' where name ='campus_entire_topic';
update topics set name = 'Floor' where name ='campus_floor_topic';
update topics set name = 'Room' where name ='campus_room_topic';
update topics set name = 'All classes' where name ='school_class_entire_year_topic';
update topics set name = 'Class' where name ='school_class_topic';
update topics set name = 'Cicle' where name ='school_entire_cicle_topic';
update topics set name = 'Faculty' where name ='school_entire_faculty_topic';
update topics set name = 'Group' where name ='school_group_topic';

update topics set name = 'Cycle' where name ='Cicle';

CREATE DOMAIN recurrence AS TEXT
CHECK ( VALUE IN ( 'none', 'daily', 'weekly', 'monthly' ) );

INSERT INTO events (starts_at, ends_at, recurrence)
SELECT ts::timestamp,      
       ts::timestamp + dur::interval,
       recur
  FROM (
    SELECT '2015-07-01'::date + i || ' ' || CASE i % 5
               WHEN 0 THEN '06:00'
               WHEN 1 THEN '10:00'
               WHEN 2 THEN '14:00'
               WHEN 3 THEN '18:00'
               ELSE        '22:30'
               END,
           CASE i % 6
               WHEN 0 THEN '2 hours'
               WHEN 1 THEN '1 hour'
               WHEN 2 THEN '45 minutes'
               WHEN 3 THEN '3.5 hours'
               WHEN 4 THEN '15 minutes'
               ELSE        '30 minutes'
               END,
           CASE i % 4
               WHEN 0 THEN 'daily'
               WHEN 1 THEN 'weekly'
               WHEN 2 THEN 'monthly'
               ELSE        'none'
               END
    FROM generate_series(1, 1000) as gen(i)
  ) AS ser( ts, dur, recur);