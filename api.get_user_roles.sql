﻿-- Function: api.get_user_roles(text)

-- DROP FUNCTION api.get_user_roles(text);

CREATE OR REPLACE FUNCTION api.get_user_roles(IN pemail text)
  RETURNS TABLE(username character varying, role character varying) AS
$BODY$BEGIN

return query (select (select case when api.get_user_type_id(pemail) = 1 then 
				(select email from student_enrollments where email = pemail)
                           when api.get_user_type_id(pemail) = 2 then 
				(select email from faculties_staff f join faculties_jobs j on f.job_id = j.id 
					where j.name in ('PROFESOR UNIV.', 'CONFERENTIAR UNIV.', 'LECTOR UNIVERSITAR') and email = pemail)
		      end) as username, 
				r.name as role
		from users u join roles r on r.id = u.role_id where u.id = (select id from users where id = api.get_user_id(pemail)));

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION api.get_user_roles(text)
  OWNER TO postgres;
