﻿-- Function: api.get_user_type_name(text)

-- DROP FUNCTION api.get_user_type_name(text);

CREATE OR REPLACE FUNCTION api.get_user_type_name(pemail text)
  RETURNS text AS
$BODY$
declare tmp integer;
BEGIN
	select id into tmp from student_enrollments where email = pemail;

	if tmp is not null
	then
		return 'student';
	else
		return 'teacher';
	end if;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION api.get_user_type_name(text)
  OWNER TO postgres;
