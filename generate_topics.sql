﻿-- Function: api.generate_topics()

-- DROP FUNCTION api.generate_topics();

CREATE OR REPLACE FUNCTION api.generate_topics()
  RETURNS boolean AS
$BODY$
	students_cursor = plpy.cursor("select u.id, e.email from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id and r.name = 'student'")
	while True:
		students = students_cursor.fetch(100)
		if not students:
			break
		for student in students:
			topics_query = plpy.prepare("""
				select 
				(select encode(digest(campus_number::text || room_number::text, 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as campus_room_topic,
				(select encode(digest(campus_number::text || substr(room_number::text, 1, 1), 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as campus_floor_topic,
				(select encode(digest(campus_number::text, 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as campus_dorm_topic, 
				(select encode(digest((select complex from campuses where id = campus_number), 'sha1'), 'hex') from student_accomodations a where a.email = e.email) as campus_entire_topic, 
				encode(digest(facultatea || grupa::text, 'sha1'), 'hex') as school_group_topic,
				encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant || domeniul || an_de_studiu || programul_de_studii, 'sha1'), 'hex') as school_class_topic,
				encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant || domeniul || an_de_studiu, 'sha1'), 'hex') as school_class_entire_year_topic,
				encode(digest(facultatea, 'sha1'), 'hex') as school_entire_faculty_topic,
				encode(digest(facultatea || ciclul_de_invatamant || forma_de_invatamant, 'sha1'), 'hex') as school_entire_cicle_topic
				from users u join student_enrollments e on u.user_id = e.id join roles r on r.id = u.role_id 
				where email = lower($1) and r.name = 'student'
					""", ['text'])
			topics_response = plpy.execute(topics_query, [student['email']])
			columns = topics_response.colnames()
			for topic in topics_response:				
				for column in columns:
					if topic[column]:
						ensure_topics_query = plpy.prepare("""						
							insert into topics (name, address, type_id) 
							select $2, $1, (select id from topic_types where type = split_part($2, '_', 1)) where not exists (select id from topics where address = $1) returning id
						""", ["text", "text"])
						#plpy.notice(topic[column], column)
						topic_insert_result = plpy.execute(ensure_topics_query, [topic[column], column])
						if not topic_insert_result:
							topic_qry = plpy.prepare("select id from topics where address = $1", ["text"])
							topic_insert_result = plpy.execute(topic_qry, [topic[column]])
						
						topic_id = topic_insert_result[0]['id']
						
						user_topic_insert_query = plpy.prepare("insert into users_topics (user_id, topic_id) select $1, $2 where not exists (select * from users_topics where user_id = $1 and topic_id = $2)", ["integer", "integer"])
						plpy.execute(user_topic_insert_query, [student['id'], topic_id])
$BODY$
  LANGUAGE plpython3u VOLATILE
  COST 100;
ALTER FUNCTION api.generate_topics()
  OWNER TO postgres;
